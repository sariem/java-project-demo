-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.0-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ebookshop
CREATE DATABASE IF NOT EXISTS `ebookshop` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ebookshop`;

-- Dumping structure for table ebookshop.books
CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table ebookshop.books: ~5 rows (approximately)
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` (`id`, `title`, `author`, `price`, `qty`) VALUES
	(1001, 'Java for dummies', 'Tan Ah Teck', 7.777, 12),
	(1002, 'More Java for dummies', 'Tan Ah Teck', 22.22, 22),
	(1003, 'More Java for more dummies', 'Mohammad Ali', 33.33, 33),
	(1004, 'A Cup of Java', 'Kumar', 44.44, 44),
	(1005, 'A Teaspoon of Java', 'Kevin Jones', 55.55, 55),
	(3001, 'Gone Fishing', 'Kumar', 11.11, 11),
	(3002, 'Gone Fishing 2', 'Kumar', 22.22, 22),
	(3003, 'Gone Fishing 3', 'Kumar', 33.33, 33),
	(3004, 'Fishing 101', 'Kumar', NULL, NULL);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
